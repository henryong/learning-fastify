"use strict";
//============================================================================
const { postgraphile } = require("postgraphile");
const ConnectionFilterPlugin = require("postgraphile-plugin-connection-filter");
// ! PlugIn '@graphile/postgis' does not seem to work in the in 'appendPlugins'.
// ! Might eventually remove this plugin since it's use case is quite limited.
// const PostgisPlugin = require("@graphile/postgis/dist");
//============================================================================
/** # PostgreSQL Database Connection
 ** We are to make a connection with the full PostgreSQL database URL.
 ** In development it should be a local DB or a Dockerised PostgreSQL DB.
 ** [PostGraphile NodeJS Settings](https://www.graphile.org/postgraphile/usage-library/)
 */
//============================================================================
module.exports = async function(fastify) {
  fastify.log.info("Establishing PostgreSQL DB connection...");
  const { PGUSER, PGPASS, PGHOST, PGPORT, PGDATABASE, PGSCHEMA } = fastify.env;
  const { NODE_ENV, /*GOOGLE_APP_ENGINE_URL,*/ GOOGLE_CONNECTION_INSTANCE } = fastify.env;
  const isDevMode = NODE_ENV === "development" || false;
  const postgresConfig = {
    user            : PGUSER,
    password        : PGPASS,
    host            : PGHOST || "localhost",
    port            : PGPORT || 5432,
    database        : PGDATABASE,
    connectionString: isDevMode
      ? undefined
      : `/cloudsql/${GOOGLE_CONNECTION_INSTANCE}`
  };
  const postgraphileOpt = {
    watchPg        : isDevMode,
    retryOnInitFail: true,
    graphiql       : isDevMode,
    enhanceGraphiql: isDevMode,
    graphiqlRoute  : "/schema",
    // enableCors     : true,
    // externalUrlBase: isDevMode ? undefined: `${GOOGLE_APP_ENGINE_URL}/schema`,
    // enabling query log is useful but soon becomes a performance issue
    disableQueryLog: true,
    appendPlugins  : [ConnectionFilterPlugin]
  };
  await fastify
    .use(postgraphile(postgresConfig, PGSCHEMA, postgraphileOpt))
    .ready(err => {
      if (err) return fastify.log.error(err);
      if (isDevMode) {
        fastify.log.info(
          `Open in route ${postgraphileOpt.graphiqlRoute} to explore the APIs.`
        );
      }
    });
};
