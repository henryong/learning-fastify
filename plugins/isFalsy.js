"use strict";
const fp = require("fastify-plugin");
module.exports = fp((fastify, opts, next) => {
  fastify.decorate("isFalsy", function(_obj) {
    //* Check the object for Falsy / Truthy.
    if (_obj === undefined) return true;
    if (_obj === null) return true;
    if (Object.getPrototypeOf(_obj) === Object.prototype && Object.keys(_obj).length < 1) return true;
    if (Object.getPrototypeOf(_obj) === Array.prototype && _obj.length < 1) return true;
    if (Object.getPrototypeOf(_obj) === String.prototype && _obj.length < 1) return true;
    if (Object.getPrototypeOf(_obj) === Number.prototype) return false;
    if (Object.getPrototypeOf(_obj) === Date.prototype) return false;
    if (Object.getPrototypeOf(_obj) === Boolean.prototype) return false;
    if (Object.getPrototypeOf(_obj) === Set.prototype && _obj.size < 1) return true;
    return false;
  });
  next();
});
// Async and aWait example:
// module.exports = fp(async (fastify, opts) => {
//   fastify.decorate("someSupport", function () {
//     return console.log("hugs");
//   });
// });