# Application Backend API Server
A Fastify GraphiQL API Server that serves the GraphQL API using Fastify web server.
It uses PostGraphile as a middleware library to connect to a PostgreSQL database, generates the GraphQL schema, and serve the API.

# __`NOTE!`__
- __`DO NOT`__ update the fastify-cli to the latest version as it broke the Postgraphile libray execution.

# To run the application locally for testing
    - Development : $ npm run dev       # raw code with watcher
    - Staging     : $ npm run _start    # still raw code but with little to no logging and no graphiql interface
    - Production  : $ npm start         # bundled version of staging environment ready for Cloud

# Build the project to bundle for Docker Container
  ```bash
  # this is currently in the works to reduce the file size
  $ npm run build
  ```

# Take Note
    - To deploy to Google's App Engine, the app.yaml file is the entry config settings before executing the command 'gcloud app deploy'
    - To keep the configuration as generic as possible, lucky for us, the app.yaml uses the Dockerfile provided for the other equivalent configs.
    - The Dockerfile contains the union configs for both App Engine's app.yaml and Docker container's configs.
    - Same goes for the .dockerignore file when doing App Engine deployments.

# ToDos
- [x] Connects PostGraphile to local PostgresDB for development with GraphQL.
- [x] Registers environment variables.
- [x] All HTTP responses, especially from the GraphQL API, to be compressed with gzip/brotli compression.
- [x] Bootstrap Fastify entry file as a plugin to serve on production server.
- [x] Uses the environment variables to connect to the PostgresDB.
- [x] Environment variable `NODE_ENV` should be `development` to show logs and `production` without.
- [x] Dockerise the application using `alpine` image.
- [ ] `npm build` should bundle the NodeJS files and `npm start` should serve the bundled application only. Use Webpack.
- [ ] Import the existing database, i.e. malaysia.
- [ ] Do up a new SQL script to init the local database.
- [ ] Adhere to GraphQL's data structure.
