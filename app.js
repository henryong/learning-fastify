"use strict";
//==============================================================================
/** # Note:
 ** - Plugins autoloaded from the plugins dir are loaded asynchronously.
 ** - Use the plugins dir ONLY as a form of utils folder where ALL of the
 **   plugins would be available on the global space.
 ** - Other middlewares/plugins that require synchronous loading can just be
 **   required from their respective dirs in the main calling js file and
 **   encapsulated with fastify-plugin.
 ** - Services are also autoloaded using the same method.
 ** - As services are mostly routes, they are already in nature asynchronous.
 */
//==============================================================================
const path = require("path");
const fp = require("fastify-plugin");
const AutoLoad = require("fastify-autoload");
const compression = require("compression");
const compressionConfig = require("./configs/compression.config");
const connectToPostgresDb = require("./middlewares/connectToPostgresDb");
const registerEnv = require("./middlewares/register_env");
//==============================================================================
module.exports = function(fastify, opts, next) {
  //============================================================================
  // Custom code lives here!
  //============================================================================
  fastify
    .register(fp(registerEnv))
    .use(compression(compressionConfig))    // no encapsulation as used in ALL routes
    .register(fp(connectToPostgresDb))
    
    .register(require("fastify-cors"), {
      origin: "*",
      methods: ["GET", "POST", "HEAD"],
      maxAge: 10 * 60, // seconds.
      allowedHeaders: [
        "Origin",
        "X-Requested-With",
        "Accept",
        "Authorization",
        "X-Apollo-Tracing",
        "Content-Type",
        "Content-Length",
        "X-PostGraphile-Explain"
      ],
      exposedHeaders: ["X-GraphQL-Event-Stream"],
      optionsSuccessStatus: 200
    })
    .ready(err => {
      if (err) return fastify.log.error(err);
      fastify.log.info("-----------------------------------------------------");
    });
  //============================================================================
  // Do not touch the following lines
  //============================================================================
  // Load all plugins defined in plugins those should be support plugins
  // that are reused through your application.
  // A plugin can be a set of routes, a server decorator or whatever.
  fastify.register(AutoLoad, {
    dir: path.resolve("plugins"),
    options: Object.assign({}, opts)
  });
  // Load all plugins defined in services define your routes in one of these
  fastify.register(AutoLoad, {
    dir: path.resolve("services"),
    options: Object.assign({}, opts)
  });
  //============================================================================
  // Make sure to call next when done
  //============================================================================
  next();
  //============================================================================
};