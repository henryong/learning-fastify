const path = require("path");
const nodeExternals = require("webpack-node-externals");
// console.warn(__dirname);
module.exports = {
  // entry: path.resolve(__dirname, "/server/_server.js"),
  entry: "C:/Repositories/fastify-application-backend/_server.js",
  // mode: 'development',
  target: "node",
  externals: [nodeExternals()],
  output: {
    filename: "_server.bundled.js",
    path: path.resolve(__dirname, "dist")
  },
  resolve: {
    extensions: [".js"]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: "babel-loader"
      }
    ]
  }
};