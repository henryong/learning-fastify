# Building the Google Cloud Docker Image
- Navigate to the directory that you want to build the Google Cloud Docker Image.
- Create the `gcloudignore` file just like how you would for the `.dockerignore`.
- Run the following command to build the image `__in Powershell__`
```bash
# gcloud builds submit --tag gcr.io/[Project_ID]/[GIVE_YOUR_PROJECT_A_NAME]
$ gcloud builds submit --tag gcr.io/stately-magpie-265202/apiserver
```
- You should be able to see the build process thereafter. E.g.:
```bash
Creating temporary tarball archive of 18 file(s) totalling 14.6 KiB before compression.
Some files were not included in the source upload.

Check the gcloud log [C:\Users\HenryOng\AppData\Roaming\gcloud\logs\2020.01.16\18.26.20.022000.log] to see which files and the contents of the
default gcloudignore file used (see `$ gcloud topic gcloudignore` to learn
more).

Uploading tarball of [.] to [gs://stately-magpie-265202_cloudbuild/source/1579170380.09-b174ee2d52de46e5b34717b6ec778142.tgz]
Created [https://cloudbuild.googleapis.com/v1/projects/stately-magpie-265202/builds/b8c31a00-36a2-4a5e-8508-c5236dd9045e].
Logs are available at [https://console.cloud.google.com/gcr/builds/b8c31a00-36a2-4a5e-8508-c5236dd9045e?project=255554801231].
------------------------------------------------- REMOTE BUILD OUTPUT --------------------------------------------------
starting build "b8c31a00-36a2-4a5e-8508-c5236dd9045e"

FETCHSOURCE
Fetching storage object: gs://stately-magpie-265202_cloudbuild/source/1579170380.09-b174ee2d52de46e5b34717b6ec778142.tgz#1579170382714705
Copying gs://stately-magpie-265202_cloudbuild/source/1579170380.09-b174ee2d52de46e5b34717b6ec778142.tgz#1579170382714705...
/ [1 files][  5.5 KiB/  5.5 KiB]
Operation completed over 1 objects/5.5 KiB.
BUILD
Already have image (with digest): gcr.io/cloud-builders/docker
Sending build context to Docker daemon  25.09kB
Step 1/7 : FROM node:12-slim
12-slim: Pulling from library/node
804555ee0376: Pulling fs layer
2706bdf80250: Pulling fs layer
2466122c9e3c: Pulling fs layer
037de6ecb82b: Pulling fs layer
e3186210e153: Pulling fs layer
037de6ecb82b: Waiting
e3186210e153: Waiting
2706bdf80250: Verifying Checksum
2706bdf80250: Download complete
804555ee0376: Verifying Checksum
804555ee0376: Download complete
037de6ecb82b: Verifying Checksum
037de6ecb82b: Download complete
e3186210e153: Verifying Checksum
e3186210e153: Download complete
2466122c9e3c: Verifying Checksum
2466122c9e3c: Download complete
804555ee0376: Pull complete
2706bdf80250: Pull complete
2466122c9e3c: Pull complete
037de6ecb82b: Pull complete
e3186210e153: Pull complete
Digest: sha256:f3e427106e858149bb70c9b97e0924f035564173466b1396b1756f293dbab042
Status: Downloaded newer image for node:12-slim
 ---> 918c7b4d1cc5
Step 2/7 : WORKDIR /usr/src/app
 ---> Running in efd03789d14d
Removing intermediate container efd03789d14d
 ---> 8602702e83c2
Step 3/7 : COPY package*.json ./
 ---> d02e5ce978ce
Step 4/7 : RUN npm i --only=production
 ---> Running in e8b4efb42bb7
[91mnpm[0m[91m [0m[91mWARN[0m[91m [0m[91mdeprecated[0m[91m @types/graphql@14.5.0: This is a stub types definition. graphql provides its own type definitions, so you do not need this installed.
[0m[91mnpm[0m[91m [0m[91mWARN[0m[91m [0m[91mdeprecated[0m[91m bourne@1.1.2: This module has moved and is now available at @hapi/bourne. Please update your dependencies as this version is no longer maintained an may contain bugs and security issues.
[0m[91mnpm[0m[91m [0m[91mnotice[0m[91m created a lockfile as package-lock.json. You should commit this file.
[0m[91mnpm[0m[91m [0m[91mWARN[0m[91m @graphile/postgis@0.1.0 requires a peer of pg-sql2@^2.2.1 but none is installed. You must install peer dependencies yourself.
[0m[91mnpm[0m[91m [0m[91mWARN[0m[91m Fastify@1.0.0 No repository field.
[0m[91mnpm[0m[91m [0m[91mWARN[0m[91m [0m[91moptional[0m[91m SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.11 (node_modules/fsevents):
[0m[91mnpm[0m[91m [0m[91mWARN[0m[91m [0m[91mnotsup[0m[91m SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.11: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
[0m[91m
[0madded 507 packages from 355 contributors and audited 4275 packages in 20.26s

4 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
Removing intermediate container e8b4efb42bb7
 ---> 992c575e8598
Step 5/7 : RUN npm audit fix
 ---> Running in f295ec46aff0
[91mnpm[0m[91m [0m[91mWARN[0m[91m @graphile/postgis@0.1.0 requires a peer of pg-sql2@^2.2.1 but none is installed. You must install peer dependencies yourself.
[0m[91mnpm[0m[91m [0m[91mWARN[0m[91m Fastify@1.0.0 No repository field.
[0m[91mnpm[0m[91m [0m[91mWARN[0m[91m [0m[91moptional[0m[91m SKIPPING OPTIONAL DEPENDENCY: fsevents@2.1.2 (node_modules/tap/node_modules/fsevents):
[0m[91mnpm[0m[91m [0m[91mWARN [0m[91mnotsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.1.2: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.11 (node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.11: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})

[0madded 350 packages from 217 contributors in 11.039s

8 packages are looking for funding
  run `npm fund` for details

fixed 0 of 0 vulnerabilities in 4275 scanned packages
Removing intermediate container f295ec46aff0
 ---> c19ddc8a93fe
Step 6/7 : COPY . .
 ---> 0d5092fd641d
Step 7/7 : CMD [ "node", "_server.js" ]
 ---> Running in d63b1b9b1a0f
Removing intermediate container d63b1b9b1a0f
 ---> fdedfaa2db5e
Successfully built fdedfaa2db5e
Successfully tagged gcr.io/stately-magpie-265202/apiserver:latest
PUSH
Pushing gcr.io/stately-magpie-265202/apiserver
The push refers to repository [gcr.io/stately-magpie-265202/apiserver]
e376bcc17a66: Preparing
f0b3512f37a6: Preparing
0f6335067f26: Preparing
d0e6dbd51e70: Preparing
51461c78c69c: Preparing
c222dce294d1: Preparing
84e470fec4af: Preparing
343eb4fc7711: Preparing
62dac45972d5: Preparing
814c70fdae62: Preparing
c222dce294d1: Waiting
84e470fec4af: Waiting
343eb4fc7711: Waiting
62dac45972d5: Waiting
814c70fdae62: Waiting
d0e6dbd51e70: Pushed
51461c78c69c: Pushed
e376bcc17a66: Pushed
c222dce294d1: Pushed
84e470fec4af: Pushed
814c70fdae62: Layer already exists
62dac45972d5: Pushed
f0b3512f37a6: Pushed
343eb4fc7711: Pushed
0f6335067f26: Pushed
latest: digest: sha256:db8e440978b044db10b58d0bb183f3b0b7f7dbb2ecccb04e3a00be05e900313b size: 2413
DONE
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ID                                    CREATE_TIME                DURATION  SOURCE                                                                                           IMAGES                                            STATUS
b8c31a00-36a2-4a5e-8508-c5236dd9045e  2020-01-16T10:26:24+00:00  1M22S     gs://stately-magpie-265202_cloudbuild/source/1579170380.09-b174ee2d52de46e5b34717b6ec778142.tgz  gcr.io/stately-magpie-265202/apiserver (+1 more)  SUCCESS
```
- You should now have the gcloud image ready.
- Head over to your Google Cloud Account and check the image status.

App Engine Service Account Key ID:
e0e1b914c1647ee91e8fd2897c21eab81662b271

Cloud SQL Service Account Key ID:
fe4561087f564215af7c80a66604c2e97f639acd

Cloud SQL Proxy command to connect to Cloud SQL's Postgres:
```bash
# make sure that this command is ran on the same location as the cloud_sql_proxy.exe file.
$ .\cloud_sql_proxy.exe -instances=stately-magpie-265202:asia-southeast1:graphqlstagingenv=tcp:5432
```

Connect to your database using the psql client:
```bash
# make sure that no other local DB has the same credential
$ psql "host=127.0.0.1 port=5432 sslmode=disable dbname=postgres user=postgres"
```