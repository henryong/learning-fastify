# docker image build -t henryong/api-server .
FROM node:12-alpine

# create work dir
WORKDIR /usr/src/app

# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# Install node application dependencies and
# build the code for production
# RUN npm install --only=production
RUN npm install
# Optionally, you can run npm audit fix alongside the npm install command.

# Copy local code to the container image
COPY . .

# Setup the Node environment variables production / develpment
ENV NODE_ENV=production

# Setup the Google Clound environment variables
# Google uses this address to listen to any network over the Internet
ENV GOOGLE_CLOUD_RUN_HOST=0.0.0.0
ENV GOOGLE_CONNECTION_INSTANCE=stately-magpie-265202:asia-southeast1:graphqlstagingenv
ENV GOOGLE_APP_ENGINE_URL=https://graphqlapiserver-dot-stately-magpie-265202.appspot.com

# Setup the Postgres environment variables
ENV PGHOST=/cloudsql/stately-magpie-265202:asia-southeast1:graphqlstagingenv
ENV PGUSER=postgres
ENV PGPASS=password
ENV PGPORT=5432
ENV PGDATABASE=postgres
ENV PGSCHEMA=production

# Define application entry point and run the server
CMD ["node", "_server.js"]